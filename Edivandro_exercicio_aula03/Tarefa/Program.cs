﻿using Microsoft.Extensions.DependencyInjection;
using Tarefa.Interfaces;
using Tarefa.Model;
using Tarefa.Negocio;
using Tarefa.Repositorio;
public class Program
{
    private static void Main(string[] args)
    {
        var serviceCollection = new ServiceCollection();

        serviceCollection.AddSingleton<ITarefaRepositorio, TarefaRepositorio>();
        serviceCollection.AddSingleton<ITarefaNegocio, TarefaNegocio>();
        serviceCollection.AddSingleton<IPessoaRepositorio, PessoaRepositorio>();
        serviceCollection.AddSingleton<IPessoaNegocio, PessoaNegocio>();

        var serviceProvider = serviceCollection.BuildServiceProvider();

        var blocoNotasNegocio = serviceProvider.GetService<ITarefaNegocio>();
        var pessoaNegocio = serviceProvider.GetService<IPessoaNegocio>();

        var novaTarefa = new Tarefas
        {
            nomeTarefa = "Estudar",
            descricao = "Praticar injeção de dependencia",
            Status = StatusTarefa.andamento,
            IdNomeResponsavel = 3
        };
        var pessoa = new Pessoa
        {
            nomePessoa = "Edivandro",
            IdPessoa = 3
        };

        var pessoa1 = new Pessoa { IdPessoa = 2 };

        pessoaNegocio.registrarResponsavel(pessoa);
        blocoNotasNegocio.registrarTarefas(novaTarefa, pessoa);
        blocoNotasNegocio.alterarResponsavelTarefa(novaTarefa.IdNomeResponsavel, pessoa1.IdPessoa);
        blocoNotasNegocio.verTarefas(pessoa1.IdPessoa);

    }
}