using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tarefa.Model;

namespace Tarefa.Interfaces
{
    public interface ITarefaNegocio
    {
        void registrarTarefas(Tarefas tarefa, Pessoa idRes);
        Tarefas verTarefas(int IdNomeResponsavel);
        public void alterarResponsavelTarefa(int IdnomeDoResponsavel, int NovoResponsavel);
        
    }
}