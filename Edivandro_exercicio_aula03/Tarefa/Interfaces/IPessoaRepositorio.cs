using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tarefa.Model;

namespace Tarefa.Interfaces
{
    public interface IPessoaRepositorio
    {
       public void cadastrarResponsavel(Pessoa pessoa);
    }

}