using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tarefa.Model;

namespace Tarefa.Interfaces
{
    public interface ITarefaRepositorio
    {
       void cadastrarTarefas(Tarefas tarefa);
       void mudarResponsavelTarefa(int novoId, int IdResponsavelAntigo);
       
       Tarefas visualizarTarefas(int IdNomeResponsavel);

       
    }
}