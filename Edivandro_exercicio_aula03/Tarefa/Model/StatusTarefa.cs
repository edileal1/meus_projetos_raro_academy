namespace Tarefa.Model
{
    public enum StatusTarefa
    {
        andamento,
        cancelada,
        concluida
    }
}