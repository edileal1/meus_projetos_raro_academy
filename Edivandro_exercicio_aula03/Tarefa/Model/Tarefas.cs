using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tarefa.Model
{
    public class Tarefas
    {
        public string nomeTarefa {get; set;}
        public string descricao {get; set;}
        public StatusTarefa Status;
        
        public int IdNomeResponsavel;
        
    }
}