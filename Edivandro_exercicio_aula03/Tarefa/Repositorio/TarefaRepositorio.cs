using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tarefa.Interfaces;
using Tarefa.Model;


namespace Tarefa.Repositorio
{
    public class TarefaRepositorio : ITarefaRepositorio
    {
        
        List<Tarefas> tarefas = new List<Tarefas>();
         
        public TarefaRepositorio()
        {
           

            tarefas.Add(new Tarefas
            {
                nomeTarefa = "Estudar",
                descricao = "Praticar lógica",
                Status = StatusTarefa.andamento,
                IdNomeResponsavel = 1
                
                
            });
            tarefas.Add(new Tarefas
            {
                nomeTarefa = "Lavar louça",
                descricao = "Usar sabão de coco",
                Status = StatusTarefa.concluida,
                IdNomeResponsavel = 2
               
            });
        }

       

        public void cadastrarTarefas(Tarefas tarefa)
        {
            tarefas.Add(tarefa);
        }

        public void mudarResponsavelTarefa(int IdResponsavelAntigo, int novoId)
        {
            var tarefaMudancaDeResponsavel = tarefas.Where(tarefa => tarefa.IdNomeResponsavel == IdResponsavelAntigo).FirstOrDefault();            
            tarefaMudancaDeResponsavel.IdNomeResponsavel = novoId;           
        }

        public Tarefas visualizarTarefas(int IdnomeDoResponsavel)
        {
            return tarefas.Where(tarefas => tarefas.IdNomeResponsavel == IdnomeDoResponsavel).FirstOrDefault();
        }

    }
}