using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tarefa.Interfaces;
using Tarefa.Model;

namespace Tarefa.Repositorio
{
    public class PessoaRepositorio : IPessoaRepositorio
    {
        public List<Pessoa> pessoas = new List<Pessoa>();

        public PessoaRepositorio()
        {
            pessoas.Add(new Pessoa { nomePessoa = "Edivandro", IdPessoa = 1 });
            pessoas.Add(new Pessoa { nomePessoa = "João", IdPessoa = 2 });
        }

        public void cadastrarResponsavel(Pessoa pessoa)
        {
            pessoas.Add(pessoa);
           
        }
    }
}