using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tarefa.Interfaces;
using Tarefa.Model;

namespace Tarefa.Negocio
{
    public class TarefaNegocio : ITarefaNegocio
    {
        private readonly ITarefaRepositorio _tarefaRepositorio;
        
        public TarefaNegocio(ITarefaRepositorio tarefaRepositorio)
        {
            _tarefaRepositorio = tarefaRepositorio;
        }

        public void registrarTarefas(Tarefas tarefa, Pessoa idRes) 
        {             
            if (tarefa == null)
            {
                throw new Exception("Objeto tarefa está vazio");
            }

            if (tarefa.descricao == null)
            {
                throw new Exception("Descrição vazia");
            }

            if (tarefa.nomeTarefa == null)
            {
                throw new Exception("Nome da tarefa necessário");
            }
            if (tarefa.IdNomeResponsavel != idRes.IdPessoa)
            {
                throw new Exception("Responsável pela tarefa diferente");
            }

            else
            {
                _tarefaRepositorio.cadastrarTarefas(tarefa);
                Console.WriteLine("Tarefa criada com sucesso!");
            }
        }

        public Tarefas verTarefas(int IdNomeResponsavel)
        {
            var tarefaEncontrada = _tarefaRepositorio.visualizarTarefas(IdNomeResponsavel);
            if (tarefaEncontrada == null)
            {
                return null;
            }
            Console.WriteLine(tarefaEncontrada.nomeTarefa);
            return tarefaEncontrada;
        }
        public void alterarResponsavelTarefa(int IdnomeDoResponsavel, int NovoResponsavel)
        {
            
            if (IdnomeDoResponsavel == NovoResponsavel)
            {
                throw new NotImplementedException("Este já é o atual responsável pela tarefa");
            }
            else
            {
                _tarefaRepositorio.mudarResponsavelTarefa(IdnomeDoResponsavel, NovoResponsavel);
                Console.WriteLine("Responsável alterado com sucesso!");
            }

        }
    }
}