using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tarefa.Interfaces;
using Tarefa.Model;

namespace Tarefa.Negocio
{
    public class PessoaNegocio : IPessoaNegocio
    {
        private readonly IPessoaRepositorio _pessoaRepositorio;

        public PessoaNegocio(IPessoaRepositorio pessoaRepositorio)
        {
            _pessoaRepositorio = pessoaRepositorio;
        }

        public void registrarResponsavel(Pessoa pessoa)
        {
            if (pessoa == null)
            {
                throw new Exception("Objeto pessoa está vazio.");
            }

            if (pessoa.nomePessoa == null || pessoa.nomePessoa.Length < 3)
            {
                throw new Exception("Nome de usuário inválido.");
            }
            if (pessoa.IdPessoa == 0)
            {
                throw new Exception("Impossível cadastrar usuário com identificador 0.");
            }
            else
            {
                _pessoaRepositorio.cadastrarResponsavel(pessoa);
                Console.WriteLine("Usuário cadastrado com sucesso.");
            }
        }
    }
}