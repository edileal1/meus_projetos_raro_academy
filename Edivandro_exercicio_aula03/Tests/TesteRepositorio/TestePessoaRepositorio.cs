using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;


namespace Tests;

[TestClass]
public class TestePessoaRepositorio
{
    private IPessoaRepositorio _pessoaRepositorio;
    public TestePessoaRepositorio()
    {
        var serviceCollection = new ServiceCollection();

        serviceCollection.AddSingleton<IPessoaRepositorio, PessoaRepositorio>();
        var serviceProvider = serviceCollection.BuildServiceProvider();
        _pessoaRepositorio = serviceProvider.GetService<IPessoaRepositorio>();
    }
    [TestMethod]
    public void TesteCadastrarPessoa()
    {
        var newPessoa = new Pessoa() {nomePessoa = "Edi", IdPessoa = 1};
        _pessoaRepositorio.cadastrarResponsavel(newPessoa);
    }
}