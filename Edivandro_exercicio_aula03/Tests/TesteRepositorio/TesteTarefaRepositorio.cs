using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;


namespace Tests;

[TestClass]
public class TesteTarefaRepositorio
{   
    private ITarefaRepositorio _tarefaRepositorio;
    public TesteTarefaRepositorio()
    {
        var serviceCollection = new ServiceCollection();

        serviceCollection.AddSingleton<ITarefaRepositorio, TarefaRepositorio>();
        var serviceProvider = serviceCollection.BuildServiceProvider();
        _tarefaRepositorio = serviceProvider.GetService<ITarefaRepositorio>();
    }

    [TestMethod]
    public void TesteCadastrarTarefa()
    {
        var idtarefa = 1;
        var newtarefa = new Tarefas(){nomeTarefa = "Varrer", descricao = "Não jogar o lixo debaixo do tapete", IdNomeResponsavel = 1, Status = StatusTarefa.concluida};
        _tarefaRepositorio.cadastrarTarefas(newtarefa);
        var verificarTarefaCadastrada =_tarefaRepositorio.visualizarTarefas(idtarefa).IdNomeResponsavel;
        
        Assert.AreEqual(newtarefa.IdNomeResponsavel, verificarTarefaCadastrada);
    }
    [TestMethod]
    public void TesteBuscarTarefa()
    {
        var idTarefa = 2;
        var newtarefa = _tarefaRepositorio.visualizarTarefas(idTarefa);
        Assert.AreEqual(idTarefa, newtarefa.IdNomeResponsavel);
    }
    [TestMethod]
    public void TesteMudarResponsavel()//Verifica se o responsável da tarefa criada foi alterado
    {
        var tarefAntiga = new Tarefas(){nomeTarefa = "Varrer", descricao = "Não jogar o lixo debaixo do tapete", IdNomeResponsavel = 1, Status = StatusTarefa.concluida};
        var tarefaNova = new Tarefas(){nomeTarefa = "Correr", descricao = "Correr 2km por dia", IdNomeResponsavel = 2, Status = StatusTarefa.concluida};
        
        _tarefaRepositorio.mudarResponsavelTarefa(tarefAntiga.IdNomeResponsavel, tarefaNova.IdNomeResponsavel);

        Assert.AreNotEqual(tarefAntiga.IdNomeResponsavel, _tarefaRepositorio.visualizarTarefas(tarefAntiga.IdNomeResponsavel));
    }
}