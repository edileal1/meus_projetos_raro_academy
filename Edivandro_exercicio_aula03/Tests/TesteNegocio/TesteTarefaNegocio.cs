using Microsoft.Extensions.DependencyInjection;
namespace Tests;

[TestClass]
public class TesteTarefaNegocio
{
    private ITarefaNegocio _tarefaNegocio;

    private readonly Moq.Mock<ITarefaRepositorio> _mockTarefaRepositorio;
   


    public TesteTarefaNegocio()
    {
        _mockTarefaRepositorio = new Moq.Mock<ITarefaRepositorio>();

    }

    [TestMethod]
    public void TarefaNãoExiste() //Tarefa não existente no banco, retorna null.
    {
        var Idtarefa = 3;

        _mockTarefaRepositorio.Setup(repositorio => repositorio.visualizarTarefas(Idtarefa)).Returns((Tarefas)null);
        _tarefaNegocio = new TarefaNegocio(_mockTarefaRepositorio.Object);

        var tarefa = _tarefaNegocio.verTarefas(Idtarefa);
        Assert.IsNull(tarefa);
    }
    [TestMethod]
    public void TarefaExisteNoBanco() //Tarefa existente buscada por id
    {
        var Idtarefa = 1;

        _mockTarefaRepositorio.Setup(repositorio => repositorio.visualizarTarefas(Idtarefa)).Returns((new Tarefas { IdNomeResponsavel = Idtarefa }));
        _tarefaNegocio = new TarefaNegocio(_mockTarefaRepositorio.Object);

        var tarefa = _tarefaNegocio.verTarefas(Idtarefa);
        Assert.AreEqual(tarefa.IdNomeResponsavel, Idtarefa);
    }

    [TestMethod]
    public void RegistrarTarefaComSucesso() //Registrar uma tarefa
    {
        var tarefa = new Tarefas()
        {
            nomeTarefa = "Limpar aquário",
            descricao = "Usar carvão ativado",
            IdNomeResponsavel = 1,
            Status = StatusTarefa.andamento
        };
        var pessoa = new Pessoa()
        {
            nomePessoa = "Edivandro",
            IdPessoa = 1

        };

        _mockTarefaRepositorio.Setup(repositorio => repositorio.cadastrarTarefas(tarefa));
        _tarefaNegocio = new TarefaNegocio(_mockTarefaRepositorio.Object);

        _tarefaNegocio.registrarTarefas(tarefa, pessoa);

    }
    [TestMethod]
    public void RegistrarTarefaNull()
    {
        _mockTarefaRepositorio.Setup(repositorio => repositorio.cadastrarTarefas(null));
        _tarefaNegocio = new TarefaNegocio(_mockTarefaRepositorio.Object);

        try
        {
            _tarefaNegocio.registrarTarefas(null, null);
        }
        catch (Exception ex)
        {
            Assert.AreEqual("Objeto tarefa está vazio", ex.Message);
        }

    }
    [TestMethod]
    public void RegistrarTarefaDescricaoVazia()
    {
        var tarefa = new Tarefas() {nomeTarefa = "limpar", descricao = null };
        _mockTarefaRepositorio.Setup(repositorio => repositorio.cadastrarTarefas(tarefa));
        _tarefaNegocio = new TarefaNegocio(_mockTarefaRepositorio.Object);

        try
        {
            _tarefaNegocio.registrarTarefas(tarefa, null);
        }
        catch (Exception ex)
        {
            Assert.AreEqual("Descrição vazia", ex.Message);
        }

    }
    [TestMethod]
    public void RegistrarTarefaComNomeVazio()
    {
        var tarefaNew = new Tarefas() { nomeTarefa = null, descricao = "Test" };
        _mockTarefaRepositorio.Setup(repositorio => repositorio.cadastrarTarefas(tarefaNew));
        _tarefaNegocio = new TarefaNegocio(_mockTarefaRepositorio.Object);

        try
        {
            _tarefaNegocio.registrarTarefas(tarefaNew, null);
        }
        catch (Exception ex)
        {
            Assert.AreEqual("Nome da tarefa necessário", ex.Message);
        }

    }
    [TestMethod]
    public void RegistrarTarefaComIDResponsavelDiferenteDoIdDaTarefa() // Mensagem de excessão caso os ids sejam diferentes.
    {
        var tarefaNew = new Tarefas() { nomeTarefa = "Edi", descricao = "Test", IdNomeResponsavel = 1 };
        var pessoaResponsavel = new Pessoa() { IdPessoa = 2 };

        _mockTarefaRepositorio.Setup(repositorio => repositorio.cadastrarTarefas(tarefaNew));
        _tarefaNegocio = new TarefaNegocio(_mockTarefaRepositorio.Object);

        try
        {
            _tarefaNegocio.registrarTarefas(tarefaNew, pessoaResponsavel);
        }
        catch (Exception ex)
        {
            Assert.AreEqual("Responsável pela tarefa diferente", ex.Message);
        }

    }
    
    [TestMethod]
    public void AltererResponsavelDaTarefaPeloMesmoResponsavel() //não é possível alterar o responsável de uma tarefa sendo ele o mesmo responsável.
    {
        var tarefaNew = new Tarefas(){IdNomeResponsavel = 1};
        var pessoaResponsavel = new Pessoa (){IdPessoa = 1};

        _mockTarefaRepositorio.Setup(repositorio => repositorio.mudarResponsavelTarefa(tarefaNew.IdNomeResponsavel, pessoaResponsavel.IdPessoa));
        _tarefaNegocio = new TarefaNegocio(_mockTarefaRepositorio.Object);

        try
        {
            _tarefaNegocio.alterarResponsavelTarefa(tarefaNew.IdNomeResponsavel, pessoaResponsavel.IdPessoa);
        }
        catch (Exception ex)
        {
            Assert.AreEqual("Este já é o atual responsável pela tarefa", ex.Message);
        }

    }
}