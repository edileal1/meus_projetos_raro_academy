using Microsoft.Extensions.DependencyInjection;
namespace Tests;

[TestClass]
public class TestePessoaNegocio
{
    private IPessoaNegocio _pessoaNegocio;

    private readonly Moq.Mock<IPessoaRepositorio> _mockPessoaRepositorio;

    public TestePessoaNegocio()
    {
        _mockPessoaRepositorio = new Moq.Mock<IPessoaRepositorio>();

    }

    [TestMethod]
    public void CadastrarPessoaComSucesso()
    {
        var pessoa = new Pessoa()
        {
            nomePessoa = "Edivandro",
            IdPessoa = 1

        };

        _mockPessoaRepositorio.Setup(repositorio => repositorio.cadastrarResponsavel(pessoa));
        _pessoaNegocio = new PessoaNegocio(_mockPessoaRepositorio.Object);

        _pessoaNegocio.registrarResponsavel(pessoa);

    }
    [TestMethod]
    public void CadastrarPessoaNull() //teste objeto pessoa vazio
    {
        _mockPessoaRepositorio.Setup(repositorio => repositorio.cadastrarResponsavel(null));
        _pessoaNegocio = new PessoaNegocio(_mockPessoaRepositorio.Object);

        try
        {
            _pessoaNegocio.registrarResponsavel(null);
        }
        catch (Exception ex)
        {
            
            Assert.AreEqual("Objeto pessoa está vazio.", ex.Message);
        }
    }
    [TestMethod]
    public void CadastrarPessoaComNomeNull() 
    {
        Pessoa pessoaNomeVazio = new Pessoa(){IdPessoa = 1};
        _mockPessoaRepositorio.Setup(repositorio => repositorio.cadastrarResponsavel(pessoaNomeVazio));
        _pessoaNegocio = new PessoaNegocio(_mockPessoaRepositorio.Object);

        try
        {
            _pessoaNegocio.registrarResponsavel(pessoaNomeVazio);
        }
        catch (Exception ex)
        {
            
            Assert.AreEqual("Nome de usuário inválido.", ex.Message);
        }
    }
    [TestMethod]
    public void CadastrarPessoaComNomeMenorQueTresCaracteres() 
    {
        Pessoa pessoaNomeInvalido = new Pessoa(){nomePessoa = "An", IdPessoa = 1};
        _mockPessoaRepositorio.Setup(repositorio => repositorio.cadastrarResponsavel(pessoaNomeInvalido));
        _pessoaNegocio = new PessoaNegocio(_mockPessoaRepositorio.Object);

        try
        {
            _pessoaNegocio.registrarResponsavel(pessoaNomeInvalido);
        }
        catch (Exception ex)
        {
            
            Assert.AreEqual("Nome de usuário inválido.", ex.Message);
        }
    }
    [TestMethod]
    public void CadastrarPessoaComIdZero() 
    {
        Pessoa pessoaId0 = new Pessoa(){nomePessoa = "Ana", IdPessoa = 0};
        _mockPessoaRepositorio.Setup(repositorio => repositorio.cadastrarResponsavel(pessoaId0));
        _pessoaNegocio = new PessoaNegocio(_mockPessoaRepositorio.Object);

        try
        {
            _pessoaNegocio.registrarResponsavel(pessoaId0);
        }
        catch (Exception ex)
        {
            
            Assert.AreEqual("Impossível cadastrar usuário com identificador 0.", ex.Message);
        }
    }
}